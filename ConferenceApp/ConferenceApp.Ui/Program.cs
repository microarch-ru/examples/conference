﻿using ConferenceApp.Domain.AttractingSpeakersContext.ApplicationAggregate;
using ConferenceApp.Domain.DiningContext.CateringAggregate;
using ConferenceApp.Domain.SalesContext.BuyerAggregate;
using ConferenceApp.Domain.SalesContext.ConferenceAggregate;
using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using ConferenceApp.Domain.SpeechesContext.TimetableAggregate;

//Продажа билетов
Console.WriteLine("------------Продажа билетов----------------------------");
var fio = Fio.Create("Иванов", "Иван", "Иванович");
var phoneNumber = PhoneNumber.Create("9261112233");
var email = Email.Create("test@yandex.ru");
var address = Address.Create("Россия", "Москва", "Тверская", "5", "1");
var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123");
var buyer = Buyer.Create(fio.Value, phoneNumber.Value, email.Value, address.Value, bankCard.Value).Value;
var conference = Conference.Create("Microarch 2023", 5, 5000).Value;
conference.SellTicket(buyer);
Console.WriteLine(conference.ToString());

//Подача заявки на выступление
Console.WriteLine("------------Подача заявки на выступление---------------");
var speakerFio = Fio.Create("Иванов", "Иван", "Иванович").Value;
var speakerPhoneNumber = PhoneNumber.Create("9261112233").Value;
var speakerEmail = Email.Create("test@yandex.ru").Value;
var application = Application.Create("SOLID", "Поговорим об ООП и SOLID", speakerFio, speakerPhoneNumber, speakerEmail).Value;
application.Accept();
Console.WriteLine($"Название доклада: {application.Name}");
Console.WriteLine($"Описание: {application.Description}");
Console.WriteLine($"Докладчик: {application.Fio}");
Console.WriteLine($"Статус: {application.Status.Name}");

//Выступления
Console.WriteLine("-------------Выступления--------------------------------");
var timetable = Timetable.Create().Value;
var presentation = Presentation.Create("Тема", "Описание", speakerFio).Value;
timetable.TakeSlot(Slot.One, presentation);
Console.WriteLine($"Название доклада: {presentation.Name}");
Console.WriteLine($"Описание: {presentation.Description}");
Console.WriteLine($"Докладчик: {presentation.Fio}");
Console.WriteLine($"Идентификатор презентации: {timetable.Slots.First().PresentationId}");

//Питание
Console.WriteLine("-------------Питание-----------------------------------");
var catering = Catering.Create().Value;
var coupon = catering.IssueCoupon(buyer.Id).Value;
catering.UseCoupon(coupon);
Console.WriteLine($"Кому выдан купон: {catering.Coupons.First().ParticipantId}");
Console.WriteLine($"Статус: {catering.Coupons.First().Status.Name}");

Console.ReadLine();