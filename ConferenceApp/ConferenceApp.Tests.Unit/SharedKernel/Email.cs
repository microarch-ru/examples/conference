using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SharedKernel;

public class EmailShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var email = Email.Create("test@yandex.ru");

        //Act

        //Assert
        email.Value.Value.Should().Be("test@yandex.ru");
    }

    [Theory]
    [MemberData(nameof(WrongEmails))]
    public void ReturnErrorWhenEmailIsWrong(Result<Email, Error> email)
    {
        //Arrange

        //Act

        //Assert
        email.Error.Should().NotBeNull();
    }

    public static IEnumerable<object[]> WrongEmails()
    {
        var email151Letter = "";
        for (var i = 0; i < 151; i++) email151Letter += "a";
        email151Letter += "@yandex.ru";

        return new List<object[]>
        {
            new object[] {Email.Create("@yandex.ru")},
            new object[] {Email.Create("test.yandex.ru")},
            new object[] {Email.Create("test@yandex")},
            new object[] {Email.Create("")},
            new object[] {Email.Create(email151Letter)}
        };
    }
}