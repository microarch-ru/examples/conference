using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SharedKernel;

public class BankCardShould
{
    private readonly BankCard _bankCard;

    public BankCardShould()
    {
        _bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123").Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123");

        //Act

        //Assert
        bankCard.Value.Number.Should().Be("1111222233334444");
        bankCard.Value.Month.Should().Be(1);
        bankCard.Value.Year.Should().Be(2030);
        bankCard.Value.CardholderName.Should().Be("IVAN IVANOV");
        bankCard.Value.Cvc.Should().Be("123");
    }

    [Fact]
    public void CanHoldMoney()
    {
        //Arrange

        //Act
        _bankCard.HoldMoney(100);

        //Assert
        _bankCard.HoldedMoney.Should().Be(100);
        _bankCard.Balance.Should().Be(1000);
    }

    [Fact]
    public void ReturnErrorWhenTryToHoldZeroMoney()
    {
        //Arrange

        //Act
        var result = _bankCard.HoldMoney(0);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenTryToHoldMoreThenBalanceAmount()
    {
        //Arrange

        //Act
        var result = _bankCard.HoldMoney(2000);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanPayMoney()
    {
        //Arrange
        _bankCard.HoldMoney(100);

        //Act
        _bankCard.Pay();

        //Assert
        _bankCard.HoldedMoney.Should().Be(0);
        _bankCard.Balance.Should().Be(900);
    }

    [Theory]
    [MemberData(nameof(WrongBankCards))]
    public void ReturnErrorWhenParamsIsEmpty(Result<BankCard, Error> bankCard)
    {
        //Arrange

        //Act

        //Assert
        bankCard.IsFailure.Should().BeTrue();
        bankCard.Error.Should().NotBeNull();
    }

    [Theory]
    [InlineData(2022)]
    [InlineData(2030)]
    public void ReturnTrueWhenExpirationDateIsCorrect(int currentYear)
    {
        //Arrange
        var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123").Value;

        //Act
        var isValid = bankCard.IsValid(new DateTime(currentYear, 1, 01));

        //Assert
        isValid.Should().BeTrue();
    }

    [Fact]
    public void ReturnFalseWhenExpirationDateExpired()
    {
        //Arrange
        var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123").Value;

        //Act
        var isValid = bankCard.IsValid(new DateTime(2032, 01, 01));

        //Assert
        isValid.Should().BeFalse();
    }

    public static IEnumerable<object[]> WrongBankCards()
    {
        return new List<object[]>
        {
            new object[] {BankCard.Create("", 1, 2030, "IVAN IVANOV", "123")},
            new object[] {BankCard.Create("1111222233334444", 1, 2030, "", "123")},
            new object[] {BankCard.Create("1111222233334444", 13, 2030, "IVAN IVANOV", "123")},
            new object[] {BankCard.Create("1111222233334444", 1, 1980, "IVAN IVANOV", "123")},
            new object[] {BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "")},
            new object[] {BankCard.Create("11112222333344445555", 1, 2030, "IVAN IVANOV", "123")}
        };
    }
}