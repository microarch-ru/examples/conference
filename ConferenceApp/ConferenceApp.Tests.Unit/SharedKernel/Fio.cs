using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SharedKernel;

public class FioShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var fio = Fio.Create("Иванов", "Иван", "Иванович");

        //Act

        //Assert
        fio.Value.Name.Should().Be("Иван");
        fio.Value.Surname.Should().Be("Иванов");
        fio.Value.Patronymic.Should().Be("Иванович");
    }

    [Theory]
    [MemberData(nameof(WrongFios))]
    public void ReturnErrorWhenParamsIsEmpty(Result<Fio, Error> fio)
    {
        //Arrange

        //Act

        //Assert
        fio.Error.Code.Should().NotBeNull();
    }

    public static IEnumerable<object[]> WrongFios()
    {
        return new List<object[]>
        {
            new object[] {Fio.Create("", "Иван", "Иванович")},
            new object[] {Fio.Create("Иванов", "", "Иванович")},
            new object[] {Fio.Create("Иванов", "Иван", "")}
        };
    }

    [Theory]
    [MemberData(nameof(ShortOrLongFios))]
    public void ReturnErrorWhenParamsIsShortOrLong(Result<Fio, Error> fio)
    {
        //Arrange

        //Act

        //Assert
        fio.Error.Should().NotBeNull();
    }

    public static IEnumerable<object[]> ShortOrLongFios()
    {
        var oneHundredLetters = "";
        for (var i = 0; i < 101; i++) oneHundredLetters += "a";
        return new List<object[]>
        {
            new object[] {Fio.Create("а", "Иван", "Иванович")},
            new object[] {Fio.Create("Иванов", "а", "Иванович")},
            new object[] {Fio.Create("Иванов", "Иван", "а")},

            new object[] {Fio.Create(oneHundredLetters, "Иван", "Иванович")},
            new object[] {Fio.Create("Иванов", oneHundredLetters, "Иванович")},
            new object[] {Fio.Create("Иванов", "Иван", oneHundredLetters)}
        };
    }
}