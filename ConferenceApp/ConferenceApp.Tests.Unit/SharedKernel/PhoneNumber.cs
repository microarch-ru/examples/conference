using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SharedKernel;

public class PhoneNumberShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var phoneNumber = PhoneNumber.Create("9261112233");

        //Act

        //Assert
        phoneNumber.Value.Value.Should().Be("9261112233");
    }

    [Theory]
    [MemberData(nameof(WrongPhoneNumbers))]
    public void ReturnErrorWhenPhoneNumberIsWrong(Result<PhoneNumber, Error> phoneNumber)
    {
        //Arrange

        //Act

        //Assert
        phoneNumber.Error.Should().NotBeNull();
    }

    public static IEnumerable<object[]> WrongPhoneNumbers()
    {
        return new List<object[]>
        {
            new object[] {PhoneNumber.Create("")},
            new object[] {PhoneNumber.Create("+79261112233")},
            new object[] {PhoneNumber.Create("926111")},
            new object[] {PhoneNumber.Create("79261112233768768768767")}
        };
    }
}