using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SharedKernel;

public class AddressShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act
        var address = Address.Create("Россия", "Москва", "Тверская", "5", "1");

        //Assert
        address.Value.Country.Should().Be("Россия");
        address.Value.City.Should().Be("Москва");
        address.Value.Street.Should().Be("Тверская");
        address.Value.House.Should().Be("5");
        address.Value.Apartment.Should().Be("1");
    }

    [Theory]
    [MemberData(nameof(WrongAddreses))]
    public void ReturnErrorWhenParamsIsEmpty(Result<Address, Error> address)
    {
        //Arrange

        //Act

        //Assert
        address.Error.Should().NotBeNull();
    }

    public static IEnumerable<object[]> WrongAddreses()
    {
        return new List<object[]>
        {
            new object[] {Address.Create("Россия", "Москва", "Тверская", "5", "")},
            new object[] {Address.Create("Россия", "Москва", "Тверская", "", "1")},
            new object[] {Address.Create("Россия", "Москва", "", "5", "1")},
            new object[] {Address.Create("Россия", "", "Тверская", "5", "1")},
            new object[] {Address.Create("", "Москва", "Тверская", "5", "1")},
            new object[] {Address.Create("", "", "", "", "")}
        };
    }
}