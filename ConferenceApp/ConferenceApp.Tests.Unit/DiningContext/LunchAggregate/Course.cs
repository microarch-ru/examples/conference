using ConferenceApp.Domain.DiningContext.LunchAggregate;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.DiningContext.LunchAggregate;

public class CourseShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act
        var result = Course.Create("Щи");

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Name.Should().Be("Щи");
    }

    [Fact]
    public void ReturnErrorWhenNameIsEmpty()
    {
        //Arrange

        //Act
        var result = Course.Create("");

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanSellTicket()
    {
        //Arrange

        //Act
        var shchi = Course.Create("Щи").Value;
        var okroshka = Course.Create("Окрошка").Value;

        var barbecue = Course.Create("Шашлык").Value;
        var cutlets = Course.Create("Котлеты").Value;

        var tea = Course.Create("Чай").Value;
        var compote = Course.Create("Компот").Value;

        //Assert
        shchi.Name.Should().Be("Щи");
        okroshka.Name.Should().Be("Окрошка");

        barbecue.Name.Should().Be("Шашлык");
        cutlets.Name.Should().Be("Котлеты");

        tea.Name.Should().Be("Чай");
        compote.Name.Should().Be("Компот");
    }

    public static IEnumerable<object[]> WrongLunches()
    {
        return new List<object[]>
        {
            new object[] {Lunch.Create("", Course.Okroshka, Course.Barbecue, Course.Сompote)},
            new object[] {Lunch.Create("Combo1", null, Course.Barbecue, Course.Сompote)},
            new object[] {Lunch.Create("Combo1", Course.Okroshka, null, Course.Сompote)},
            new object[] {Lunch.Create("Combo1", Course.Okroshka, Course.Barbecue, null)}
        };
    }
}