using ConferenceApp.Domain.DiningContext.LunchAggregate;
using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.DiningContext.LunchAggregate;

public class LunchShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var firstCourse = Course.Okroshka;
        var secondCourse = Course.Barbecue;
        var drink = Course.Сompote;

        //Act
        var result = Lunch.Create("Combo1", firstCourse, secondCourse, drink);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Name.Should().Be("Combo1");
        result.Value.FirstCourse.Should().Be(firstCourse);
        result.Value.SecondCourse.Should().Be(secondCourse);
        result.Value.Drink.Should().Be(drink);
    }

    [Theory]
    [MemberData(nameof(WrongLunches))]
    public void ReturnErrorWhenParamsIsEmpty(Result<Lunch, Error> lunches)
    {
        //Arrange

        //Act

        //Assert
        lunches.IsFailure.Should().BeTrue();
        lunches.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanSellTicket()
    {
        //Arrange

        //Act
        var combo1 = Lunch.Combo1;
        var combo2 = Lunch.Combo2;

        //Assert
        combo1.Name.Should().Be("combo1");
        combo1.FirstCourse.Should().NotBeNull();
        combo1.SecondCourse.Should().NotBeNull();
        combo1.Drink.Should().NotBeNull();

        combo2.Name.Should().Be("combo2");
        combo2.FirstCourse.Should().NotBeNull();
        combo2.SecondCourse.Should().NotBeNull();
        combo2.Drink.Should().NotBeNull();
    }

    public static IEnumerable<object[]> WrongLunches()
    {
        return new List<object[]>
        {
            new object[] {Lunch.Create("", Course.Okroshka, Course.Barbecue, Course.Сompote)},
            new object[] {Lunch.Create("Combo1", null, Course.Barbecue, Course.Сompote)},
            new object[] {Lunch.Create("Combo1", Course.Okroshka, null, Course.Сompote)},
            new object[] {Lunch.Create("Combo1", Course.Okroshka, Course.Barbecue, null)}
        };
    }
}