using ConferenceApp.Domain.DiningContext.CateringAggregate;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.DiningContext.CateringAggregate;

public class CateringShould
{
    private readonly Catering _catering;

    public CateringShould()
    {
        _catering = Catering.Create().Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act
        var result = Catering.Create();

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Coupons.Count.Should().Be(0);
    }

    [Fact]
    public void CanIssueCoupon()
    {
        //Arrange
        var participantId = Guid.NewGuid();

        //Act
        var result = _catering.IssueCoupon(participantId);

        //Assert
        result.IsSuccess.Should().BeTrue();
        _catering.Coupons.Count.Should().Be(1);
    }

    [Fact]
    public void CanUseCoupon()
    {
        //Arrange
        var participant1 = Guid.NewGuid();
        var coupon1 = _catering.IssueCoupon(participant1).Value;

        var participant2 = Guid.NewGuid();
        _catering.IssueCoupon(participant2);

        //Act
        var result = _catering.UseCoupon(coupon1);

        //Assert
        result.IsSuccess.Should().BeTrue();
        _catering.Coupons.Count(c => c.Status == Status.Created).Should().Be(1);
    }

    [Fact]
    public void ReturnErrorWhenParticipantIdIsEmpty()
    {
        //Arrange
        var participantId = Guid.Empty;

        //Act
        var result = _catering.IssueCoupon(participantId);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenTryUseNotExistedCoupon()
    {
        //Arrange
        var participantId = Guid.NewGuid();
        var notExistedCoupon = Coupon.Create(participantId).Value;

        //Act
        var result = _catering.UseCoupon(notExistedCoupon);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenTryUseUsedCoupon()
    {
        //Arrange
        var participantId = Guid.NewGuid();
        var coupon = _catering.IssueCoupon(participantId).Value;
        _catering.UseCoupon(coupon);

        //Act
        var result = _catering.UseCoupon(coupon);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }
}