using ConferenceApp.Domain.DiningContext.CateringAggregate;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.DiningContext.CateringAggregate;

public class CouponShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var participantId = Guid.NewGuid();

        //Act
        var result = Coupon.Create(participantId);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.ParticipantId.Should().Be(participantId);
        result.Value.Status.Should().Be(Status.Created);
    }

    [Fact]
    public void ReturnErrorWhenParticipantIdIsEmpty()
    {
        //Arrange
        var participantId = Guid.Empty;

        //Act
        var result = Coupon.Create(participantId);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanUseCoupon()
    {
        //Arrange
        var participantId = Guid.NewGuid();
        var coupon = Coupon.Create(participantId).Value;

        //Act
        var result = coupon.Use();

        //Assert
        result.IsSuccess.Should().BeTrue();
        coupon.ParticipantId.Should().Be(participantId);
        coupon.Status.Should().Be(Status.Used);
    }
}