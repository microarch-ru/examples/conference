using ConferenceApp.Domain.SalesContext.BuyerAggregate;
using ConferenceApp.Domain.SharedKernel;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SalesContext.BuyerAggregate;

public class BuyerShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var fio = Fio.Create("Иванов", "Иван", "Иванович");
        var phoneNumber = PhoneNumber.Create("9261112233");
        var email = Email.Create("test@yandex.ru");
        var address = Address.Create("Россия", "Москва", "Тверская", "5", "1");
        var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123");

        //Act
        var buyer = Buyer.Create(fio.Value, phoneNumber.Value, email.Value, address.Value, bankCard.Value);

        //Assert
        buyer.IsSuccess.Should().BeTrue();

        buyer.Value.Fio.Name.Should().Be("Иван");
        buyer.Value.Fio.Surname.Should().Be("Иванов");
        buyer.Value.Fio.Patronymic.Should().Be("Иванович");

        buyer.Value.PhoneNumber.Value.Should().Be("9261112233");
        buyer.Value.Email.Value.Should().Be("test@yandex.ru");

        buyer.Value.Address.Country.Should().Be("Россия");
        buyer.Value.Address.City.Should().Be("Москва");
        buyer.Value.Address.Street.Should().Be("Тверская");
        buyer.Value.Address.House.Should().Be("5");
        buyer.Value.Address.Apartment.Should().Be("1");

        buyer.Value.BankCard.Number.Should().Be("1111222233334444");
        buyer.Value.BankCard.Month.Should().Be(1);
        buyer.Value.BankCard.Year.Should().Be(2030);
        buyer.Value.BankCard.CardholderName.Should().Be("IVAN IVANOV");
        buyer.Value.BankCard.Cvc.Should().Be("123");
    }
}