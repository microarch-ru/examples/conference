using ConferenceApp.Domain.SalesContext.BuyerAggregate;
using ConferenceApp.Domain.SalesContext.ConferenceAggregate;
using ConferenceApp.Domain.SharedKernel;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SalesContext.ConferenceAggregate;

public class TicketShould
{
    private readonly Buyer _buyer;

    public TicketShould()
    {
        //Arrange Buyer
        var fio = Fio.Create("Иванов", "Иван", "Иванович");
        var phoneNumber = PhoneNumber.Create("9261112233");
        var email = Email.Create("test@yandex.ru");
        var address = Address.Create("Россия", "Москва", "Тверская", "5", "1");
        var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123");
        _buyer = Buyer.Create(fio.Value, phoneNumber.Value, email.Value, address.Value, bankCard.Value).Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act
        var result = Ticket.Create(1000);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Price.Should().Be(1000);
        result.Value.Status.Should().Be(Status.Free);
    }

    [Fact]
    public void ReturnErrorWhenPriceIsEmpty()
    {
        //Arrange

        //Act
        var result = Ticket.Create(0);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }


    [Fact]
    public void CanBookTicket()
    {
        //Arrange
        var ticket = Ticket.Create(1000).Value;

        //Act
        ticket.Book(_buyer.Id);

        //Assert
        ticket.Status.Should().Be(Status.Booked);
        ticket.BuyerId.Should().Be(_buyer.Id);
    }

    [Fact]
    public void ReturnErrorWhenTryToBookTicketWithEmptyBuyerId()
    {
        //Arrange
        var ticket = Ticket.Create(1000).Value;

        //Act
        var result = ticket.Book(Guid.Empty);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanSellTicketWhenTicketWasBooked()
    {
        //Arrange
        var ticket = Ticket.Create(1000).Value;
        ticket.Book(_buyer.Id);

        //Act
        ticket.Sell();

        //Assert
        ticket.Status.Should().Be(Status.Sold);
        ticket.BuyerId.Should().Be(_buyer.Id);
    }

    [Fact]
    public void ReturnErrorWhenTryToSellAndTicketWasNotBooked()
    {
        //Arrange
        var ticket = Ticket.Create(1000).Value;

        //Act
        var result = ticket.Sell();

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
        ticket.Status.Should().Be(Status.Free);
    }


    [Fact]
    public void CanCancelTicket()
    {
        //Arrange
        var ticket = Ticket.Create(1000).Value;
        ticket.Book(_buyer.Id);
        ticket.Sell();

        //Act
        ticket.Cancel();

        //Assert
        ticket.Status.Should().Be(Status.Free);
        ticket.BuyerId.Should().BeNull();
    }

    [Fact]
    public void CanPrintInformation()
    {
        //Arrange
        var ticket = Ticket.Create(1000).Value;
        ticket.Book(_buyer.Id);
        ticket.Sell();

        //Act
        var info = ticket.ToString();

        //Assert
        info.Should().Be($"Идентификатор билета: {ticket.Id}\n" +
                         $"Идентификатор покупателя: {ticket.BuyerId}\n" +
                         $"Цена билета: {ticket.Price}\n" +
                         $"Статус: {ticket.Status.Name}");
    }
}