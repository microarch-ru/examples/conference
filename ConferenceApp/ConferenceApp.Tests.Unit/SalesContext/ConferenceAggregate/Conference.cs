using ConferenceApp.Domain.SalesContext.BuyerAggregate;
using ConferenceApp.Domain.SalesContext.ConferenceAggregate;
using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SalesContext.ConferenceAggregate;

public class ConferenceShould
{
    private readonly Buyer _buyer;

    public ConferenceShould()
    {
        //Arrange Buyer
        var fio = Fio.Create("Иванов", "Иван", "Иванович");
        var phoneNumber = PhoneNumber.Create("9261112233");
        var email = Email.Create("test@yandex.ru");
        var address = Address.Create("Россия", "Москва", "Тверская", "5", "1");
        var bankCard = BankCard.Create("1111222233334444", 1, 2030, "IVAN IVANOV", "123");
        _buyer = Buyer.Create(fio.Value, phoneNumber.Value, email.Value, address.Value, bankCard.Value).Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act
        var conference = Conference.Create("Microarch 2023", 1000, 5000);

        //Assert
        conference.IsSuccess.Should().BeTrue();
        conference.Value.Name.Should().Be("Microarch 2023");
        conference.Value.QuantityOfTickets.Should().Be(1000);
    }

    [Theory]
    [MemberData(nameof(WrongConferences))]
    public void ReturnErrorWhenParamsIsEmpty(Result<Conference, Error> address)
    {
        //Arrange

        //Act

        //Assert
        address.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanSellTicket()
    {
        //Arrange

        //Act
        var conference = Conference.Create("Microarch 2023", 1000, 5000).Value;
        conference.SellTicket(_buyer);

        //Assert
        conference.Tickets.Count.Should().Be(1);

        var ticket = conference.Tickets.FirstOrDefault();
        ticket?.BuyerId.Should().Be(_buyer.Id);
        ticket?.Status.Should().Be(Status.Sold);
    }

    [Fact]
    public void ReturnErrorWhenTryToSellTicketButAllTicketsIsSoldOut()
    {
        //Arrange
        var conference = Conference.Create("Microarch 2023", 1, 5000).Value;
        conference.SellTicket(_buyer);

        //Act
        var result = conference.SellTicket(_buyer);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanCancelTicket()
    {
        //Arrange
        var conference = Conference.Create("Microarch 2023", 1000, 5000).Value;
        var ticketId = conference.SellTicket(_buyer).Value;

        //Act
        conference.CancelTicket(ticketId);

        //Assert
        conference.Tickets.Count.Should().Be(0);
    }

    [Fact]
    public void ThrowErrorWhenCancelTicketAndTicketIdIsEmpty()
    {
        //Arrange
        var conference = Conference.Create("Microarch 2023", 1000, 5000).Value;
        var ticketId = Guid.Empty;

        //Act
        var result = conference.CancelTicket(ticketId);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanPrintInformation()
    {
        //Arrange
        var conference = Conference.Create("Microarch 2023", 1000, 5000).Value;
        var ticketId = conference.SellTicket(_buyer).Value;

        //Act
        var info = conference.ToString();

        //Assert
        info.Should()
            .Be($"Название конференции: Microarch 2023\n" +
                $"Количество доступных билетов: 1000\n" +
                $"Продано билетов: 1\n" +
                $"---\n" +
                    $"Идентификатор билета: {ticketId}\n" +
                    $"Идентификатор покупателя: {_buyer.Id}\n" +
                    $"Цена билета: 5000\n" +
                    $"Статус: sold"+
                $"\n"+
                $"---\n");
    }

    public static IEnumerable<object[]> WrongConferences()
    {
        return new List<object[]>
        {
            new object[] {Conference.Create("", 1000, 5000)},
            new object[] {Conference.Create("Microarch 2023", 0, 5000)},
            new object[] {Conference.Create("Microarch 2023", 1000, 0)}
        };
    }
}