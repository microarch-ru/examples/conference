using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using ConferenceApp.Domain.SpeechesContext.TimetableAggregate;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SpeechesContext.TimetableAggregate;

public class TimetableShould
{
    private readonly Presentation _presentation;
    private readonly Timetable _timetable;

    public TimetableShould()
    {
        var fio = Fio.Create("Иванов", "Иван", "Иванович").Value;
        _presentation = Presentation.Create("Тактические паттерны DDD", "Мы рассмотрим Aggregate, VO, Entity", fio)
            .Value;
        _timetable = Timetable.Create().Value;
    }

    [Fact]
    public void HaveEightSlots()
    {
        //Arrange
        var timetable = Timetable.Create();

        //Act

        //Assert
        timetable.IsSuccess.Should().BeTrue();
        timetable.Value.Slots.Should().NotBeEmpty();
        timetable.Value.Slots.Count.Should().Be(8);
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var timetable = Timetable.Create();

        //Act

        //Assert
        timetable.IsSuccess.Should().BeTrue();
        timetable.Value.Slots.Should().NotBeEmpty();
    }

    [Fact]
    public void CanTakeFreeSlot()
    {
        //Arrange
        var timetable = Timetable.Create().Value;
        var fio = Fio.Create("Иванов", "Иван", "Иванович").Value;
        var presentation = Presentation
            .Create("Тактические паттерны DDD", "Мы рассмотрим Aggregate, VO, Entity", fio).Value;

        //Act
        var slot = timetable.Slots.First();
        var result = timetable.TakeSlot(slot, presentation);

        //Assert
        result.IsSuccess.Should().BeTrue();
        slot.PresentationId.Should().Be(presentation.Id);
    }

    [Fact]
    public void ReturnErrorWhenTakeSlotAndSlotIsWrong()
    {
        //Arrange

        //Act
        var result = _timetable.TakeSlot(null, _presentation);

        //Assert
        result.IsFailure.Should().BeTrue();
    }

    [Fact]
    public void ReturnErrorWhenTakeSlotAndSlotIsBusy()
    {
        //Arrange
        var slot = _timetable.Slots.First();
        _timetable.TakeSlot(slot, _presentation);

        //Act
        var result = _timetable.TakeSlot(slot, _presentation);

        //Assert
        result.IsFailure.Should().BeTrue();
    }

    [Fact]
    public void CanClearSlot()
    {
        //Arrange
        var slot = _timetable.Slots.First();
        _timetable.TakeSlot(slot, _presentation);

        //Act
        var result = _timetable.ClearSlot(slot);

        //Assert
        result.IsSuccess.Should().BeTrue();
        slot.PresentationId.Should().BeNull();
    }


    [Fact]
    public void ReturnErrorWhenClearSlotAndSlotIsWrong()
    {
        //Arrange

        //Act
        var result = _timetable.ClearSlot(null);

        //Assert
        result.IsFailure.Should().BeTrue();
    }
}