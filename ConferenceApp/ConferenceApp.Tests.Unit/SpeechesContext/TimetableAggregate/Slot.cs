﻿using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using ConferenceApp.Domain.SpeechesContext.TimetableAggregate;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SpeechesContext.TimetableAggregate;

public class SlotShould
{
    private readonly Presentation _presentation;

    public SlotShould()
    {
        var fio = Fio.Create("Иванов", "Иван", "Иванович").Value;
        _presentation = Presentation.Create("Тактические паттерны DDD", "Мы рассмотрим Aggregate, VO, Entity", fio)
            .Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act
        var slot = Slot.Create(10, 11);

        //Assert
        slot.IsSuccess.Should().BeTrue();
        slot.Value.From.Should().Be(10);
        slot.Value.To.Should().Be(11);
    }

    [Fact]
    public void ReturnErrorWhenFromAndToTheSame()
    {
        //Arrange

        //Act
        var slot = Slot.Create(10, 10);

        //Assert
        slot.IsFailure.Should().BeTrue();
        slot.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenFromMoreThenTo()
    {
        //Arrange

        //Act
        var slot = Slot.Create(11, 10);

        //Assert
        slot.IsFailure.Should().BeTrue();
        slot.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenFromIsEmpty()
    {
        //Arrange

        //Act
        var slot = Slot.Create(0, 10);

        //Assert
        slot.IsFailure.Should().BeTrue();
        slot.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenToIsEmpty()
    {
        //Arrange

        //Act
        var slot = Slot.Create(10, 0);

        //Assert
        slot.IsFailure.Should().BeTrue();
        slot.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanReturnAllSlotes()
    {
        //Arrange
        var slots = Slot.List();

        //Act

        //Assert
        slots.Should().NotBeEmpty();
    }

    [Fact]
    public void CanAddPresentation()
    {
        //Arrange
        var slot = Slot.Create(10, 11).Value;

        //Act
        slot.AddPresentation(_presentation);

        //Assert
        slot.PresentationId.Should().Be(_presentation.Id);
    }

    [Fact]
    public void CanClearSlot()
    {
        //Arrange
        var slot = Slot.Create(10, 11).Value;
        slot.AddPresentation(_presentation);

        //Act
        slot.ClearSlot();

        //Assert
        slot.PresentationId.Should().BeNull();
    }
}