using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SpeechesContext.PresentationAggregate;

public class PresentationShould
{
    private readonly Fio _fio;
    private readonly Presentation _presentation;

    public PresentationShould()
    {
        _fio = Fio.Create("Иванов", "Иван", "Иванович").Value;
        _presentation = Presentation.Create("Тактические паттерны DDD", "Мы рассмотрим Aggregate, VO, Entity", _fio)
            .Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange

        //Act

        //Assert
        _presentation.Name.Should().Be("Тактические паттерны DDD");
        _presentation.Description.Should().Be("Мы рассмотрим Aggregate, VO, Entity");
        _presentation.Fio.Should().BeEquivalentTo(_fio);

        _presentation.Status.Should().Be(Status.Created);
        _presentation.Score.Should().Be(Score.None);
        _presentation.Comment.Should().BeEmpty();
    }

    [Fact]
    public void RerurnErrorWhenNameIsEmpty()
    {
        //Arrange
        
        //Act
        var presentation = Presentation.Create("", "Мы рассмотрим Aggregate, VO, Entity", _fio);

        //Assert
        presentation.IsFailure.Should().BeTrue();
        presentation.Error.Should().NotBeNull();
    }

    [Fact]
    public void RerurnErrorWhenDescriptionsIsEmpty()
    {
        //Arrange

        //Act
        var presentation = Presentation.Create("Тактические паттерны DDD", "", _fio);

        //Assert
        presentation.IsFailure.Should().BeTrue();
        presentation.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanBeEstimeteWhenFinished()
    {
        //Arrange
        _presentation.MarkAsCompleted();

        //Act
        _presentation.Estimate(Score.Five, "Супер!");

        //Assert
        _presentation.Score.Should().Be(Score.Five);
        _presentation.Comment.Should().Be("Супер!");
    }

    [Fact]
    public void RerurnErrorWhenTryingEstimeteWhenNotFinished()
    {
        //Arrange

        //Act
        var result = _presentation.Estimate(Score.Five, "Супер!");

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void RerurnErrorWhenTryingEstimeteAndScoreIsWrong()
    {
        //Arrange
        _presentation.MarkAsCompleted();

        //Act
        var result = _presentation.Estimate(Score.None, "Супер!");

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void RerurnErrorWhenTryingEstimeteAndCommentIsEmpty()
    {
        //Arrange
        _presentation.MarkAsCompleted();

        //Act
        var result = _presentation.Estimate(Score.Five, "");

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }
}