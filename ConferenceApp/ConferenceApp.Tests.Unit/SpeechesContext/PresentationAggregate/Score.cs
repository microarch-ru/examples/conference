using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.SpeechesContext.PresentationAggregate;

public class ScoreShould
{
    [Fact]
    public void CanReturnAllStatuses()
    {
        //Arrange
        var statuses = Score.List();

        //Act

        //Assert
        statuses.Should().NotBeEmpty();
    }

    [Theory]
    [MemberData(nameof(Scores))]
    public void HaveCorrectStatusNameAndId(Result<Score, Error> scores, int id, string name)
    {
        //Arrange

        //Act

        //Assert
        scores.Value.Id.Should().Be(id);
        scores.Value.Name.Should().Be(name);
    }

    public static IEnumerable<object[]> Scores()
    {
        return new List<object[]>
        {
            new object[] {Score.None, 0, "none"},
            new object[] {Score.One, 1, "one"},
            new object[] {Score.Two, 2, "two"},
            new object[] {Score.Three, 3, "three"},
            new object[] {Score.Four, 4, "four"},
            new object[] {Score.Five, 5, "five"}
        };
    }

    [Theory]
    [MemberData(nameof(Scores))]
    public void ReturnCorrectStatusById(Result<Score, Error> score, int id, string name)
    {
        //Arrange

        //Act

        //Assert
        Score.From(id).Should().BeEquivalentTo(score.Value);
    }

    [Theory]
    [MemberData(nameof(Scores))]
    public void ReturnCorrectStatusByName(Result<Score, Error> score, int id, string name)
    {
        //Arrange

        //Act

        //Assert
        Score.FromName(name).Should().BeEquivalentTo(score.Value);
    }

    [Fact]
    public void ThrowExceptionWhenStatusIdIsWrong()
    {
        //Arrange

        //Act
        Action act = () => Score.From(-1);

        //Assert
        act.Should().Throw<ArgumentException>();
    }

    [Fact]
    public void ThrowExceptionWhenStatusNameIsWrong()
    {
        //Arrange

        //Act
        Action act = () => Score.FromName("");

        //Assert
        act.Should().Throw<ArgumentException>();
    }
}