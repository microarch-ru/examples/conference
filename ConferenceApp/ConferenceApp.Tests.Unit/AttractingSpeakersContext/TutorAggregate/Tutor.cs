using ConferenceApp.Domain.AttractingSpeakersContext.TutorAggregate;
using ConferenceApp.Domain.SharedKernel;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.AttractingSpeakersContext.TutorAggregate;

public class TutorShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var fio = Fio.Create("Иванов", "Иван", "Иванович");

        //Act
        var tutor = Tutor.Create(fio.Value);

        //Assert
        tutor.IsSuccess.Should().BeTrue();

        tutor.Value.Fio.Name.Should().Be("Иван");
        tutor.Value.Fio.Surname.Should().Be("Иванов");
        tutor.Value.Fio.Patronymic.Should().Be("Иванович");
    }
}