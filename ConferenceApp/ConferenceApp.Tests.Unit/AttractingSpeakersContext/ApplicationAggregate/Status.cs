using ConferenceApp.Domain.AttractingSpeakersContext.ApplicationAggregate;
using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.AttractingSpeakersContext.ApplicationAggregate;

public class StatusShould
{
    [Fact]
    public void CanReturnAllStatuses()
    {
        //Arrange
        var statuses = Status.List();

        //Act

        //Assert
        statuses.Should().NotBeEmpty();
    }

    [Theory]
    [MemberData(nameof(Statuses))]
    public void HaveCorrectStatusNameAndId(Result<Status, Error> statuses, int id, string name)
    {
        //Arrange

        //Act

        //Assert
        statuses.Value.Id.Should().Be(id);
        statuses.Value.Name.Should().Be(name);
    }

    public static IEnumerable<object[]> Statuses()
    {
        return new List<object[]>
        {
            new object[] {Status.Created, 1, "created"},
            new object[] {Status.SubmittedForReview, 2, "submittedforreview"},
            new object[] {Status.Accepted, 3, "accepted"},
            new object[] {Status.Canceled, 4, "canceled"}
        };
    }

    [Theory]
    [MemberData(nameof(Statuses))]
    public void ReturnCorrectStatusById(Result<Status, Error> status, int id, string name)
    {
        //Arrange

        //Act

        //Assert
        Status.From(id).Should().BeEquivalentTo(status.Value);
    }

    [Theory]
    [MemberData(nameof(Statuses))]
    public void ReturnCorrectStatusByName(Result<Status, Error> status, int id, string name)
    {
        //Arrange

        //Act

        //Assert
        Status.FromName(name).Should().BeEquivalentTo(status.Value);
    }

    [Fact]
    public void ThrowExceptionWhenStatusIdIsWrong()
    {
        //Arrange

        //Act
        Action act = () => Status.From(-1);

        //Assert
        act.Should().Throw<ArgumentException>();
    }

    [Fact]
    public void ThrowExceptionWhenStatusNameIsWrong()
    {
        //Arrange

        //Act
        Action act = () => Status.FromName("");

        //Assert
        act.Should().Throw<ArgumentException>();
    }
}