using ConferenceApp.Domain.AttractingSpeakersContext.ApplicationAggregate;
using ConferenceApp.Domain.SharedKernel;
using FluentAssertions;

namespace ConferenceApp.Tests.Unit.AttractingSpeakersContext.ApplicationAggregate;

public class ApplicationShould
{
    private readonly Application _application;
    private readonly Email _email;
    private readonly Fio _fio;
    private readonly PhoneNumber _phoneNumber;

    public ApplicationShould()
    {
        _fio = Fio.Create("Иванов", "Иван", "Иванович").Value;
        _phoneNumber = PhoneNumber.Create("9261112233").Value;
        _email = Email.Create("test@yandex.ru").Value;

        _application = Application.Create("Тактические паттерны DDD", "Мы рассмотрим Aggregate, VO, Entity", _fio,
            _phoneNumber, _email).Value;
    }

    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var fio = Fio.Create("Иванов", "Иван", "Иванович");
        var phoneNumber = PhoneNumber.Create("9261112233");
        var email = Email.Create("test@yandex.ru");

        //Act
        var application = Application.Create("Тактические паттерны DDD", "Мы рассмотрим Aggregate, VO, Entity",
            fio.Value, phoneNumber.Value, email.Value);

        //Assert
        application.IsSuccess.Should().BeTrue();

        application.Value.Fio.Name.Should().Be("Иван");
        application.Value.Fio.Surname.Should().Be("Иванов");
        application.Value.Fio.Patronymic.Should().Be("Иванович");

        application.Value.PhoneNumber.Value.Should().Be("9261112233");
        application.Value.Email.Value.Should().Be("test@yandex.ru");

        application.Value.Name.Should().Be("Тактические паттерны DDD");
        application.Value.Description.Should().Be("Мы рассмотрим Aggregate, VO, Entity");

        application.Value.Status.Should().Be(Status.Created);
    }

    [Fact]
    public void ReturnErrorWhenCreatedWithEmptyName()
    {
        //Arrange

        //Act
        var result = Application.Create("", "Мы рассмотрим Aggregate, VO, Entity", _fio, _phoneNumber, _email);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void ReturnErrorWhenCreatedWithEmptyDescription()
    {
        //Arrange

        //Act
        var result = Application.Create("Тактические паттерны DDD", "", _fio, _phoneNumber, _email);

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }

    [Fact]
    public void CanSubmitForReview()
    {
        //Arrange

        //Act
        var result = _application.SubmitForReview();

        //Assert
        result.IsSuccess.Should().BeTrue();
        _application.Status.Should().Be(Status.SubmittedForReview);
    }

    [Fact]
    public void CanAcceptAfterReview()
    {
        //Arrange
        _application.SubmitForReview();

        //Act
        var result = _application.Accept();


        //Assert
        result.IsSuccess.Should().BeTrue();
        _application.Status.Should().Be(Status.Accepted);
    }

    [Fact]
    public void CanCancelAfterReview()
    {
        //Arrange
        _application.SubmitForReview();

        //Act
        var result = _application.Cancel("Непонятно");

        //Assert
        result.IsSuccess.Should().BeTrue();
        _application.Status.Should().Be(Status.Canceled);
        _application.Reason.Should().Be("Непонятно");
    }

    [Fact]
    public void ReturnErrorWhenCancelWithEmptyReason()
    {
        //Arrange
        _application.SubmitForReview();

        //Act
        var result = _application.Cancel("");

        //Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().NotBeNull();
    }
}