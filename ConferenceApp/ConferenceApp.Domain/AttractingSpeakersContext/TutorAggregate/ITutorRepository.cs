﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.AttractingSpeakersContext.TutorAggregate;

/// <summary>
///     Контракт репозитория для Tutor
/// </summary>
public interface ITutorRepository : IRepository<Tutor>
{
    Tutor Add(Tutor tutor);
    Tutor Update(Tutor tutor);
    Task<Tutor> FindAsync(Guid Id);
    Task<Tutor> FindByIdAsync(Guid Id);
}

