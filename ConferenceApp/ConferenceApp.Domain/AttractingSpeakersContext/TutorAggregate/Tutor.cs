using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.AttractingSpeakersContext.TutorAggregate;

/// <summary>
///     Куратор
/// </summary>
public class Tutor : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Ctr
    /// </summary>
    protected Tutor()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="fio">ФИО</param>
    private Tutor(Fio fio) : this()
    {
        Id = Guid.NewGuid();
        Fio = fio;
    }

    /// <summary>
    ///     ФИО
    /// </summary>
    public Fio Fio { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="fio">ФИО</param>
    /// <returns>Результат</returns>
    public static Result<Tutor, Error> Create(Fio fio)
    {
        return new Tutor(fio);
    }
}