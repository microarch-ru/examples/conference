﻿using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.AttractingSpeakersContext.ApplicationAggregate;

/// <summary>
///     Статус заявки
/// </summary>
public class Status : Entity<int>
{
    public static readonly Status Created = new(1, nameof(Created).ToLowerInvariant());
    public static readonly Status SubmittedForReview = new(2, nameof(SubmittedForReview).ToLowerInvariant());
    public static readonly Status Accepted = new(3, nameof(Accepted).ToLowerInvariant());
    public static readonly Status Canceled = new(4, nameof(Canceled).ToLowerInvariant());

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Status(int id, string name)
    {
        Id = id;
        Name = name;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    public static IEnumerable<Status> List()
    {
        return new[] {Created, SubmittedForReview, Accepted, Canceled};
    }

    public static Status FromName(string name)
    {
        var state = List()
            .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Status)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }

    public static Status From(int id)
    {
        var state = List().SingleOrDefault(s => s.Id == id);

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Status)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }
}
