﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.AttractingSpeakersContext.ApplicationAggregate;

/// <summary>
///     Контракт репозитория для Application
/// </summary>
public interface IApplicationRepository : IRepository<Application>
{
    Application Add(Application application);
    Application Update(Application application);
    Task<Application> FindAsync(Guid Id);
    Task<Application> FindByIdAsync(Guid Id);
}