using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.AttractingSpeakersContext.ApplicationAggregate;

/// <summary>
///     Заявка на выступление
/// </summary>
public class Application : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Ctr
    /// </summary>
    protected Application()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="name">Название</param>
    /// <param name="description">Описание доклада</param>
    /// <param name="fio">ФИО</param>
    /// <param name="phoneNumber">Номер телефона</param>
    /// <param name="email">Email</param>
    private Application(string name, string description, Fio fio, PhoneNumber phoneNumber, Email email) : this()
    {
        Id = Guid.NewGuid();
        Name = name;
        Description = description;
        Fio = fio;
        PhoneNumber = phoneNumber;
        Email = email;
        Status = Status.Created;
    }

    /// <summary>
    ///     Название доклада
    /// </summary>
    public string Name { get; protected set; }

    /// <summary>
    ///     Описание доклада
    /// </summary>
    public string Description { get; protected set; }

    /// <summary>
    ///     Причина в случае отклонения
    /// </summary>
    public string Reason { get; protected set; }

    /// <summary>
    ///     ФИО заявителя
    /// </summary>
    public Fio Fio { get; protected set; }

    /// <summary>
    ///     Номер телефона заявителя
    /// </summary>
    public PhoneNumber PhoneNumber { get; protected set; }

    /// <summary>
    ///     Email заявителя
    /// </summary>
    public Email Email { get; protected set; }

    /// <summary>
    ///     Статус заявки
    /// </summary>
    public Status Status { get; protected set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="name">Название</param>
    /// <param name="description">Описание доклада</param>
    /// <param name="fio">ФИО</param>
    /// <param name="phoneNumber">Номер телефона</param>
    /// <param name="email">Email</param>
    /// <returns>Результат</returns>
    public static Result<Application, Error> Create(string name, string description, Fio fio, PhoneNumber phoneNumber,
        Email email)
    {
        if (string.IsNullOrWhiteSpace(name))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(description))
            return Errors.General.ValueIsRequired();

        return new Application(name, description, fio, phoneNumber, email);
    }

    /// <summary>
    ///     Отправить заявку на рассмотрение
    /// </summary>
    public Result<object, Error> SubmitForReview()
    {
        Status = Status.SubmittedForReview;
        return new object();
    }

    /// <summary>
    ///     Принять заявку
    /// </summary>
    public Result<object, Error> Accept()
    {
        Status = Status.Accepted;
        return new object();
    }

    /// <summary>
    ///     Отклонить заявку
    /// </summary>
    /// <param name="reason">Причина</param>
    public Result<object, Error> Cancel(string reason)
    {
        if (string.IsNullOrWhiteSpace(reason))
            return Errors.General.ValueIsRequired();
        Reason = reason;

        Status = Status.Canceled;
        return new object();
    }
}