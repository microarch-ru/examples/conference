﻿using System.Diagnostics.CodeAnalysis;
using System.Text;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SharedKernel;

/// <summary>
///     ФИО
/// </summary>
public class Fio : ValueObject
{
    /// <summary>
    ///     Ctr
    /// </summary>
    private Fio(string surname, string name, string patronymic)
    {
        Surname = surname;
        Name = name;
        Patronymic = patronymic;
    }

    /// <summary>
    ///     Фамилия
    /// </summary>
    public string Surname { get; }

    /// <summary>
    ///     Имя
    /// </summary>
    public string Name { get; }

    /// <summary>
    ///     Отчество
    /// </summary>
    public string Patronymic { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="surname">Фамилия</param>
    /// <param name="name">Имя</param>
    /// <param name="patronymic">Отчество</param>
    /// <returns>Результат</returns>
    public static Result<Fio, Error> Create(string surname, string name, string patronymic)
    {
        if (string.IsNullOrWhiteSpace(surname))
            return Errors.General.ValueIsRequired();
        if (surname.Length <= 1 || surname.Length > 100)
            return Errors.General.InvalidLength(nameof(surname));

        if (string.IsNullOrWhiteSpace(name))
            return Errors.General.ValueIsRequired();
        if (name.Length <= 1 || name.Length > 100)
            return Errors.General.InvalidLength(nameof(name));

        if (string.IsNullOrWhiteSpace(patronymic))
            return Errors.General.ValueIsRequired();
        if (patronymic.Length <= 1 || patronymic.Length > 100)
            return Errors.General.InvalidLength(nameof(patronymic));

        return new Fio(surname, name, patronymic);
    }
    
    /// <summary>
    ///     Получить информацию о ФИО
    /// </summary>
    /// <returns>Информация</returns>
    public override string ToString()
    {
        var info = new StringBuilder();
        info.Append($"{Surname} ");
        info.Append($"{Name} ");
        info.Append(Patronymic);
        return info.ToString();
    }

    [ExcludeFromCodeCoverage]
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Surname;
        yield return Name;
        yield return Patronymic;
    }
}
