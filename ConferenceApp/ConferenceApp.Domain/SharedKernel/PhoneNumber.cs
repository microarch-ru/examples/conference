using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SharedKernel;

/// <summary>
///     Номер телефона
/// </summary>
public class PhoneNumber : ValueObject
{
    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="value">Значение</param>
    private PhoneNumber(string value)
    {
        Value = value;
    }

    /// <summary>
    ///     Значение
    /// </summary>
    public string Value { get; }

    /// <summary>
    ///     Создание
    /// </summary>
    /// <param name="input">Значение</param>
    /// <returns>Результат</returns>
    public static Result<PhoneNumber, Error> Create(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
            return Errors.General.ValueIsRequired();

        var phoneNumber = input.Trim();

        if (!Regex.IsMatch(phoneNumber, @"^[0-9]{10}$"))
            return Errors.General.ValueIsInvalid();

        return new PhoneNumber(phoneNumber);
    }

    [ExcludeFromCodeCoverage]
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}