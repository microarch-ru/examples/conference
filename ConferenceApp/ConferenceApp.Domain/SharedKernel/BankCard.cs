﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SharedKernel;

/// <summary>
///     Банковская карта
/// </summary>
public class BankCard : ValueObject
{
    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="number">Номер</param>
    /// <param name="month">Месяц</param>
    /// <param name="year">Год</param>
    /// <param name="cardholderName">Имя держателя</param>
    /// <param name="cvc">Cvc код</param>
    private BankCard(string number, int month, int year, string cardholderName, string cvc)
    {
        Number = number;
        Month = month;
        Year = year;
        CardholderName = cardholderName;
        Cvc = cvc;
        Balance = 1000;
        HoldedMoney = 0;
    }

    /// <summary>
    ///     Номер
    /// </summary>
    public string Number { get; }

    /// <summary>
    ///     Месяц
    /// </summary>
    public int Month { get; }

    /// <summary>
    ///     Год
    /// </summary>
    public int Year { get; }

    /// <summary>
    ///     Имя держателя
    /// </summary>
    public string CardholderName { get; }

    /// <summary>
    ///     Cvc код
    /// </summary>
    public string Cvc { get; }

    /// <summary>
    ///     Баланс
    /// </summary>
    public decimal Balance { get; protected set; }

    /// <summary>
    ///     Заблокированная сумма
    /// </summary>
    public decimal HoldedMoney { get; protected set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="number">Номер</param>
    /// <param name="month">Месяц</param>
    /// <param name="year">Год</param>
    /// <param name="cardholderName">Имя держателя</param>
    /// <param name="cvc">Cvc код</param>
    /// <returns>Результат</returns>
    public static Result<BankCard, Error> Create(string number, int month, int year, string cardholderName, string cvc)
    {
        if (string.IsNullOrWhiteSpace(number))
            return Errors.General.ValueIsRequired();
        number = number.Trim();

        if (!Regex.IsMatch(number, @"^\d{9,18}$"))
            return Errors.General.ValueIsInvalid();

        if (string.IsNullOrWhiteSpace(cardholderName))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(cvc))
            return Errors.General.ValueIsRequired();

        if (month is > 12 or < 1)
            return Errors.General.ValueIsInvalid();

        if (year is < 1990)
            return Errors.General.ValueIsInvalid();

        return new BankCard(number, month, year, cardholderName, cvc);
    }

    /// <summary>
    ///     Не истекла ли карта
    /// </summary>
    /// <param name="curentDateTime">Текущая дата</param>
    /// <returns>Да/Нет</returns>
    public bool IsValid(DateTime curentDateTime)
    {
        var currentMonth = curentDateTime.Month;
        var currentYear = curentDateTime.Year;

        if (Year > currentYear) return true;

        if (Year == currentYear && Month >= currentMonth) return true;
        return false;
    }

    /// <summary>
    ///     Заблокировать сумму на карте
    /// </summary>
    /// <param name="amount">Сумма</param>
    public Result<object, Error> HoldMoney(decimal amount)
    {
        if (amount <= 0)
            return Errors.General.ValueIsInvalid();
        if (GetFreeMoney().Value < amount)
            return Errors.Conference.NotEnoughMoney();
        HoldedMoney += amount;
        return new object();
    }

    /// <summary>
    ///     Доступные деньги
    /// </summary>
    /// <returns>Свободные средства</returns>
    public Result<decimal, Error> GetFreeMoney()
    {
        return Balance - HoldedMoney;
    }

    /// <summary>
    ///     Заблокировать сумму на карте
    /// </summary>
    public Result<object, Error> Pay()
    {
        Balance -= HoldedMoney;
        HoldedMoney = 0;
        return new object();
    }

    [ExcludeFromCodeCoverage]
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Number;
        yield return Month;
        yield return Year;
        yield return CardholderName;
        yield return Cvc;
    }
}
