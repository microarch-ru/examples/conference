﻿using System.Diagnostics.CodeAnalysis;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SharedKernel;

/// <summary>
///     Адрес
/// </summary>
public class Address : ValueObject
{
    /// <summary>
    ///     Ctr
    /// </summary>
    private Address(string country, string city, string street, string house, string apartment)
    {
        Country = country;
        City = city;
        Street = street;
        House = house;
        Apartment = apartment;
    }

    /// <summary>
    ///     Страна
    /// </summary>
    public string Country { get; }

    /// <summary>
    ///     Город
    /// </summary>
    public string City { get; }

    /// <summary>
    ///     Улица
    /// </summary>
    public string Street { get; }

    /// <summary>
    ///     Дом
    /// </summary>
    public string House { get; }

    /// <summary>
    ///     Квартира
    /// </summary>
    public string Apartment { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="country">Страна</param>
    /// <param name="city">Город</param>
    /// <param name="street">Улица</param>
    /// <param name="house">Дом</param>
    /// <param name="apartment">Квартира</param>
    /// <returns>Результат</returns>
    public static Result<Address, Error> Create(string country, string city, string street, string house,
        string apartment)
    {
        if (string.IsNullOrWhiteSpace(country))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(city))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(street))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(house))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(apartment))
            return Errors.General.ValueIsRequired();

        return new Address(country, city, street, house, apartment);
    }

    [ExcludeFromCodeCoverage]
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Country;
        yield return City;
        yield return Street;
        yield return House;
        yield return Apartment;
    }
}
