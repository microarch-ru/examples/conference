using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SharedKernel;

/// <summary>
///     Email
/// </summary>
public class Email : ValueObject
{
    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="value">Значение</param>
    private Email(string value)
    {
        Value = value;
    }

    /// <summary>
    ///     Значение
    /// </summary>
    public string Value { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="input">Значение</param>
    /// <returns>Результат</returns>
    public static Result<Email, Error> Create(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
            return Errors.General.ValueIsRequired();

        var email = input.Trim();

        if (email.Length > 150)
            return Errors.General.InvalidLength();

        if (!Regex.IsMatch(email, @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"))
            return Errors.General.ValueIsInvalid();

        return new Email(email);
    }

    [ExcludeFromCodeCoverage]
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}