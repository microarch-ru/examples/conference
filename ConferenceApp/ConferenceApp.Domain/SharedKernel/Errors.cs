using System.Diagnostics.CodeAnalysis;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SharedKernel;

[ExcludeFromCodeCoverage]
public sealed class Error : ValueObject
{
    private const string Separator = "||";

    internal Error(string code, string message)
    {
        Code = code;
        Message = message;
    }

    /// <summary>
    /// Код ошибки
    /// </summary>
    public string Code { get; }
    
    /// <summary>
    ///  Текст ошибки
    /// </summary>
    public string Message { get; }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Code;
    }

    public string Serialize()
    {
        return $"{Code}{Separator}{Message}";
    }

    public static Error Deserialize(string serialized)
    {
        if (serialized == "A non-empty request body is required.")
            return Errors.General.ValueIsRequired();

        var data = serialized.Split(new[] {Separator}, StringSplitOptions.RemoveEmptyEntries);

        if (data.Length < 2)
            throw new FormatException($"Invalid error serialization: '{serialized}'");

        return new Error(data[0], data[1]);
    }
}

/// <summary>
///  Ошибки конкретных доменных сущностей
/// </summary>
[ExcludeFromCodeCoverage]
public static class Errors
{
    public static class Conference
    {
        public static Error NotEnoughMoney()
        {
            return new("conference.not.enough.money", "Не достаточно денег на карте");
        }

        public static Error AllTicketsSold()
        {
            return new("conference.all.tickets.sold", "Нет доступных для продажи билетов, все билеты проданы");
        }
    }

    public static class Presentation
    {
        public static Error PresentationShouldBeFinished()
        {
            return new("presentation.should.be.finished", "Презентация должна быть завершена");
        }
    }

    public static class TimeTable
    {
        public static Error SlotIsBusy()
        {
            return new("slot.is.busy", "Слот занят. Выберите другой слот.");
        }
    }

    public static class Ticket
    {
        public static Error TicketShouldBeBooked()
        {
            return new("ticket.should.be.booked", "Билет должен быть забронирован перед оплатой");
        }
    }

    /// <summary>
    ///  Общие ошибки
    /// </summary>
    public static class General
    {
        public static Error NotFound(long? id = null)
        {
            var forId = id == null ? "" : $" for Id '{id}'";
            return new Error("record.not.found", $"Record not found{forId}");
        }

        public static Error ValueIsInvalid()
        {
            return new("value.is.invalid", "Value is invalid");
        }

        public static Error ValueIsRequired()
        {
            return new("value.is.required", "Value is required");
        }

        public static Error InvalidLength(string? name = null)
        {
            var label = name == null ? " " : " " + name + " ";
            return new Error("invalid.string.length", $"Invalid{label}length");
        }

        public static Error CollectionIsTooSmall(int min, int current)
        {
            return new Error(
                "collection.is.too.small",
                $"The collection must contain {min} items or more. It contains {current} items.");
        }

        public static Error CollectionIsTooLarge(int max, int current)
        {
            return new Error(
                "collection.is.too.large",
                $"The collection must contain {max} items or more. It contains {current} items.");
        }

        public static Error InternalServerError(string message)
        {
            return new Error("internal.server.error", message);
        }
    }
}