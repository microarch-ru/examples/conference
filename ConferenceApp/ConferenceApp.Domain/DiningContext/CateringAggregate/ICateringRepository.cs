﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.DiningContext.CateringAggregate;

/// <summary>
///     Контракт репозитория для Catering
/// </summary>
public interface ICateringRepository : IRepository<Catering>
{
    Catering Add(Catering catering);
    Catering Update(Catering catering);
    Task<Catering> FindAsync(Guid Id);
    Task<Catering> FindByIdAsync(Guid Id);
}