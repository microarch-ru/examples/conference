using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.DiningContext.CateringAggregate;

/// <summary>
///     Кухня
/// </summary>
public class Catering : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Выданные талоны на обед
    /// </summary>
    private readonly List<Coupon> _coupons;

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Catering()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    private Catering(Guid id) : this()
    {
        Id = id;
        _coupons = new List<Coupon>();
    }

    public IReadOnlyCollection<Coupon> Coupons => _coupons;

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <returns>Результат</returns>
    public static Result<Catering, Error> Create()
    {
        var id = Guid.NewGuid();
        return new Catering(id);
    }

    /// <summary>
    ///     Использовать талон на обед
    /// </summary>
    /// <param name="lunch">Обед</param>
    /// <param name="coupon">Талон</param>
    public Result<object, Error> UseCoupon(Coupon coupon)
    {
        var existedCoupon = _coupons.SingleOrDefault(c => c.Id == coupon.Id);
        if (existedCoupon == null) return Errors.General.NotFound();
        if (existedCoupon.Status == Status.Used) return Errors.Conference.AllTicketsSold();
        existedCoupon.Use();

        return new object();
    }

    /// <summary>
    ///     Выдать талон на обед
    /// </summary>
    /// <param name="participantId">Идентификатор участника конференции</param>
    public Result<Coupon, Error> IssueCoupon(Guid participantId)
    {
        if (participantId == Guid.Empty)
            return Errors.General.ValueIsInvalid();

        var coupon = Coupon.Create(participantId);
        if (coupon.IsFailure) return coupon.Error;
        _coupons.Add(coupon.Value);
        return coupon.Value;
    }
}