using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.DiningContext.CateringAggregate;

/// <summary>
///     Талон на питание
/// </summary>
public class Coupon : Entity<Guid>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    protected Coupon()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="participantId">Идентификатор участника конференции</param>
    private Coupon(Guid participantId) : this()
    {
        Id = Guid.NewGuid();
        ParticipantId = participantId;
        Status = Status.Created;
    }

    /// <summary>
    ///     Статус
    /// </summary>
    public Status Status { get; protected set; }

    /// <summary>
    ///     Идентификатор участника конференции
    /// </summary>
    public Guid? ParticipantId { get; protected set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="participantId">Идентификатор участника конференции</param>
    /// <returns>Результат</returns>
    public static Result<Coupon, Error> Create(Guid participantId)
    {
        if (participantId == Guid.Empty)
            return Errors.General.ValueIsInvalid();

        return new Coupon(participantId);
    }

    /// <summary>
    ///     Использовать
    /// </summary>
    public Result<object, Error> Use()
    {
        Status = Status.Used;
        return new object();
    }
}