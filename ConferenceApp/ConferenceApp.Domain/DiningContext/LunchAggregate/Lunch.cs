using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.DiningContext.LunchAggregate;

/// <summary>
///     Обед
/// </summary>
public class Lunch : Entity<Guid>,IAggregateRoot
{
    public static readonly Lunch Combo1 = new(nameof(Combo1).ToLowerInvariant(), Course.Shchi, Course.Barbecue, Course.Сompote);
    public static readonly Lunch Combo2 = new(nameof(Combo2).ToLowerInvariant(), Course.Okroshka, Course.Сutlets, Course.Tea);

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Lunch()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="name">Цена</param>
    /// <param name="firstCourse">Первое блюдо</param>
    /// <param name="secondCourse">Второе блюдо</param>
    /// <param name="drink">Напиток</param>
    private Lunch(string name, Course firstCourse, Course secondCourse, Course drink) : this()
    {
        Id = Guid.NewGuid();
        Name = name;
        FirstCourse = firstCourse;
        SecondCourse = secondCourse;
        Drink = drink;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    /// <summary>
    ///     Первое блюдо
    /// </summary>
    public Course FirstCourse { get; }

    /// <summary>
    ///     Второе блюдо
    /// </summary>
    public Course SecondCourse { get; }

    /// <summary>
    ///     Напиток
    /// </summary>
    public Course Drink { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="name">Цена</param>
    /// <param name="firstCourse">Первое блюдо</param>
    /// <param name="secondCourse">Второе блюдо</param>
    /// <param name="drink">Напиток</param>
    public static Result<Lunch, Error> Create(string name, Course firstCourse, Course secondCourse, Course drink)
    {
        if (string.IsNullOrWhiteSpace(name))
            return Errors.General.ValueIsRequired();

        if (firstCourse == null)
            return Errors.General.ValueIsRequired();

        if (secondCourse == null)
            return Errors.General.ValueIsRequired();

        if (drink == null)
            return Errors.General.ValueIsRequired();

        return new Lunch(name, firstCourse, secondCourse, drink);
    }
}