﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.DiningContext.LunchAggregate;

/// <summary>
///     Контракт репозитория для Course
/// </summary>
public interface IDishRepository : IRepository<Course>
{
    Course Add(Course course);
    Course Update(Course course);
    Task<Course> FindAsync(Guid Id);
    Task<Course> FindByIdAsync(Guid Id);
}

