using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.DiningContext.LunchAggregate;

/// <summary>
///     Блюдо
/// </summary>
public class Course : Entity<Guid>, IAggregateRoot
{
    public static readonly Course Shchi = new("Щи");
    public static readonly Course Okroshka = new("Окрошка");

    public static readonly Course Barbecue = new("Шашлык");
    public static readonly Course Сutlets = new("Котлеты");

    public static readonly Course Tea = new("Чай");
    public static readonly Course Сompote = new("Компот");

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Course()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="name">Название</param>
    private Course(string name) : this()
    {
        Id = Guid.NewGuid();
        Name = name;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="name">Название</param>
    /// <returns>Результат</returns>
    public static Result<Course, Error> Create(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            return Errors.General.ValueIsRequired();

        return new Course(name);
    }
}