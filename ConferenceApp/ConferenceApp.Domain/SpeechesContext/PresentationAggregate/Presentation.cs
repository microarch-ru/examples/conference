using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SpeechesContext.PresentationAggregate;

/// <summary>
///     Доклад
/// </summary>
public class Presentation : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Ctr
    /// </summary>
    protected Presentation()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="name">Название</param>
    /// <param name="description">Описание доклада</param>
    /// <param name="speaker">Спикер</param>
    /// <param name="fio">ФИО спикера</param>
    private Presentation(string name, string description, Fio fio) : this()
    {
        Id = Guid.NewGuid();
        Name = name;
        Description = description;
        Fio = fio;
        Status = Status.Created;
        Score = Score.None;
        Comment = "";
    }

    /// <summary>
    ///     Название доклада
    /// </summary>
    public string Name { get; }

    /// <summary>
    ///     Описание доклада
    /// </summary>
    public string Description { get; }

    /// <summary>
    ///     ФИО спикера
    /// </summary>
    public Fio Fio { get; }

    /// <summary>
    ///     Статус выступления
    /// </summary>
    public Status Status { get; protected set; }

    /// <summary>
    ///     Оценка выступления
    /// </summary>
    public Score Score { get; protected set; }

    /// <summary>
    ///     Комментарий к оценке
    /// </summary>
    public string Comment { get; protected set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="name">Название</param>
    /// <param name="description">Описание доклада</param>
    /// <param name="fio">ФИО спикера</param>
    /// <returns>Результат</returns>
    public static Result<Presentation, Error> Create(string name, string description, Fio fio)
    {
        if (string.IsNullOrWhiteSpace(name))
            return Errors.General.ValueIsRequired();

        if (string.IsNullOrWhiteSpace(description))
            return Errors.General.ValueIsRequired();

        return new Presentation(name, description, fio);
    }

    /// <summary>
    ///     Пометить как завершенный
    /// </summary>
    public Result<object, Error> MarkAsCompleted()
    {
        Status = Status.Finished;
        return new object();
    }

    /// <summary>
    ///     Оценить доклад
    /// </summary>
    /// <param name="score">Оценка</param>
    /// <param name="comment">Комментарий к оценке</param>
    public Result<object, Error> Estimate(Score score, string comment)
    {
        if (Status != Status.Finished)
            return Errors.Presentation.PresentationShouldBeFinished();

        if (score.Equals(Score.None))
            return Errors.General.ValueIsRequired();
        Score = score;

        if (string.IsNullOrWhiteSpace(comment))
            return Errors.General.ValueIsRequired();
        Comment = comment;

        return new object();
    }
}