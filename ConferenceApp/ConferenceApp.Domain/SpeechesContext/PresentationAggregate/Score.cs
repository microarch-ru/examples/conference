﻿using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SpeechesContext.PresentationAggregate;

/// <summary>
///     Оценка выступления
/// </summary>
public class Score : Entity<int>
{
    public static readonly Score None = new(0, nameof(None).ToLowerInvariant());
    public static readonly Score One = new(1, nameof(One).ToLowerInvariant());
    public static readonly Score Two = new(2, nameof(Two).ToLowerInvariant());
    public static readonly Score Three = new(3, nameof(Three).ToLowerInvariant());
    public static readonly Score Four = new(4, nameof(Four).ToLowerInvariant());
    public static readonly Score Five = new(5, nameof(Five).ToLowerInvariant());

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Score(int id, string name)
    {
        Id = id;
        Name = name;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    public static IEnumerable<Score> List()
    {
        return new[] {None, One, Two, Three, Four, Five};
    }

    public static Score FromName(string name)
    {
        var state = List()
            .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Score)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }

    public static Score From(int id)
    {
        var state = List().SingleOrDefault(s => s.Id == id);

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Score)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }
}