﻿using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SpeechesContext.PresentationAggregate;

/// <summary>
///     Статус выступления
/// </summary>
public class Status : Entity<int>
{
    public static readonly Status Created = new(1, nameof(Created).ToLowerInvariant());
    public static readonly Status Finished = new(2, nameof(Finished).ToLowerInvariant());

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Status(int id, string name)
    {
        Id = id;
        Name = name;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    public static IEnumerable<Status> List()
    {
        return new[] {Created, Finished};
    }

    public static Status FromName(string name)
    {
        var state = List()
            .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Status)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }

    public static Status From(int id)
    {
        var state = List().SingleOrDefault(s => s.Id == id);

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Status)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }
}
