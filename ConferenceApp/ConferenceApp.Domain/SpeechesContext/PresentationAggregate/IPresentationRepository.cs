﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.SpeechesContext.PresentationAggregate;

/// <summary>
///     Контракт репозитория для Presentation
/// </summary>
public interface IPresentationRepository : IRepository<Presentation>
{
    Presentation Add(Presentation presentation);
    Presentation Update(Presentation presentation);
    Task<Presentation> FindAsync(Guid Id);
    Task<Presentation> FindByIdAsync(Guid Id);
}

