using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SpeechesContext.TimetableAggregate;

/// <summary>
///     Сетка конференции
/// </summary>
public class Timetable : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Слоты
    /// </summary>
    private readonly List<Slot> _slots;

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Timetable()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="slots">Слоты</param>
    private Timetable(List<Slot> slots) : this()
    {
        Id = Guid.NewGuid();
        _slots = slots;
    }

    public IReadOnlyCollection<Slot> Slots => _slots;

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="name">Название</param>
    /// <returns>Результат</returns>
    public static Result<Timetable, Error> Create()
    {
        var slots = new List<Slot>
        {
            Slot.One,
            Slot.Two,
            Slot.Three,
            Slot.Four,
            Slot.Five,
            Slot.Six,
            Slot.Seven,
            Slot.Eight
        };

        return new Timetable(slots);
    }

    /// <summary>
    ///     Занять слот
    /// </summary>
    /// <param name="slot">Слот</param>
    /// <param name="presentation">Presentation</param>
    public Result<object, Error> TakeSlot(Slot slot, Presentation presentation)
    {
        var existedSlot = _slots.SingleOrDefault(c => c.Id == slot?.Id);

        if (existedSlot == null)
            return Errors.General.NotFound();

        if (existedSlot.PresentationId != null)
            return Errors.TimeTable.SlotIsBusy();

        existedSlot.AddPresentation(presentation);
        return new object();
    }

    /// <summary>
    ///     Освободить слот
    /// </summary>
    /// <param name="slot">Слот</param>
    public Result<object, Error> ClearSlot(Slot slot)
    {
        var existedSlot = _slots.SingleOrDefault(c => c.Id == slot?.Id);
        if (existedSlot == null) return Errors.General.NotFound();

        existedSlot.ClearSlot();
        return new object();
    }
}