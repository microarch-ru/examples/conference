﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.SpeechesContext.TimetableAggregate;

/// <summary>
///     Контракт репозитория для Timetable
/// </summary>
public interface ITimetableRepository : IRepository<Timetable>
{
    Timetable Add(Timetable timetable);
    Timetable Update(Timetable timetable);
    Task<Timetable> FindAsync(Guid Id);
    Task<Timetable> FindByIdAsync(Guid Id);
}