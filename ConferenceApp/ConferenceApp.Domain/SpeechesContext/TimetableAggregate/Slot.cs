using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Domain.SpeechesContext.PresentationAggregate;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SpeechesContext.TimetableAggregate;

/// <summary>
///     Слот
/// </summary>
public class Slot : Entity<Guid>
{
    public static readonly Slot One = new(10, 11);
    public static readonly Slot Two = new(11, 12);
    public static readonly Slot Three = new(12, 13);
    public static readonly Slot Four = new(13, 14);
    public static readonly Slot Five = new(14, 15);
    public static readonly Slot Six = new(15, 16);
    public static readonly Slot Seven = new(16, 17);
    public static readonly Slot Eight = new(17, 18);

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Slot()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="from">Время начала</param>
    /// <param name="to">Время конца</param>
    private Slot(int from, int to) : this()
    {
        Id = Guid.NewGuid();
        From = from;
        To = to;
    }

    /// <summary>
    ///     Время начала
    /// </summary>
    public int From { get; protected set; }

    /// <summary>
    ///     Время конца
    /// </summary>
    public int To { get; protected set; }

    /// <summary>
    ///     Идентификатор доклада
    /// </summary>
    public Guid? PresentationId { get; protected set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="from">Время начала</param>
    /// <param name="to">Время конца</param>
    /// <returns>Результат</returns>
    public static Result<Slot, Error> Create(int from, int to)
    {
        if (from <= 0)
            return Errors.General.ValueIsInvalid();

        if (to <= 0)
            return Errors.General.ValueIsInvalid();

        if (from >= to)
            return Errors.General.ValueIsInvalid();

        return new Slot(from, to);
    }

    public static IEnumerable<Slot> List()
    {
        return new[] {One, Two, Three, Four, Five, Six, Seven, Eight};
    }

    /// <summary>
    ///     Добавить доклад в слот
    /// </summary>
    /// <param name="presentation">Presentation</param>
    public Result<object, Error> AddPresentation(Presentation presentation)
    {
        PresentationId = presentation.Id;
        return new object();
    }

    /// <summary>
    ///     Освободить слот
    /// </summary>
    /// <param name="slot">Слот</param>
    public Result<object, Error> ClearSlot()
    {
        PresentationId = null;
        return new object();
    }
}