﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.SalesContext.ConferenceAggregate;

/// <summary>
///     Контракт репозитория для Conference
/// </summary>
public interface IConferenceRepository : IRepository<Conference>
{
    Conference Add(Conference сonference);
    Conference Update(Conference сonference);
    Task<Conference> FindAsync(Guid Id);
    Task<Conference> FindByIdAsync(Guid Id);
}