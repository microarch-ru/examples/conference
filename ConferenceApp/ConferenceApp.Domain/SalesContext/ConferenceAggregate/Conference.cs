using System.Text;
using ConferenceApp.Domain.SalesContext.BuyerAggregate;
using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SalesContext.ConferenceAggregate;

/// <summary>
///     Конференция
/// </summary>
public class Conference : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Проданные или забронированные билеты
    /// </summary>
    private readonly List<Ticket> _tickets;

    /// <summary>
    ///     Ctr
    /// </summary>
    protected Conference()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="name">Название</param>
    /// <param name="quantityOfTickets">Количество билетов</param>
    /// <param name="oneTicketPrice">Стоимость одного билета</param>
    private Conference(string name, int quantityOfTickets, decimal oneTicketPrice) : this()
    {
        Id = Guid.NewGuid();
        Name = name;
        QuantityOfTickets = quantityOfTickets;
        OneTicketPrice = oneTicketPrice;
        _tickets = new List<Ticket>();
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    /// <summary>
    ///     Количество доступных билетов
    /// </summary>
    public int QuantityOfTickets { get; private set; }

    /// <summary>
    ///     Стоимость одного билета
    /// </summary>
    public decimal OneTicketPrice { get; }

    public IReadOnlyCollection<Ticket> Tickets => _tickets;

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="name">Название</param>
    /// <param name="quantityOfTickets">Количество билетов</param>
    /// <param name="oneTicketPrice">Стоимость одного билета</param>
    /// <returns>Результат</returns>
    public static Result<Conference, Error> Create(string name, int quantityOfTickets, decimal oneTicketPrice)
    {
        if (string.IsNullOrWhiteSpace(name))
            return Errors.General.ValueIsRequired();

        if (quantityOfTickets <= 0)
            return Errors.General.ValueIsInvalid();

        if (oneTicketPrice <= 0)
            return Errors.General.ValueIsInvalid();

        return new Conference(name, quantityOfTickets, oneTicketPrice);
    }

    /// <summary>
    ///     Оплатить билет
    /// </summary>
    /// <param name="buyer">Покупатель</param>
    public Result<Guid, Error> SellTicket(Buyer buyer)
    {
        if (_tickets.Count + 1 > QuantityOfTickets) return Errors.Conference.AllTicketsSold();

        var ticket = Ticket.Create(OneTicketPrice);
        if (ticket.IsFailure) return ticket.Error;

        ticket.Value.Book(buyer.Id);
        ticket.Value.Sell();

        _tickets.Add(ticket.Value);
        return ticket.Value.Id;
    }

    /// <summary>
    ///     Отменить бронь билета
    /// </summary>
    public Result<object, Error> CancelTicket(Guid ticketId)
    {
        var ticket = _tickets.SingleOrDefault(c => c.Id == ticketId);
        if (ticket == null) return Errors.General.NotFound();

        _tickets.Remove(ticket);
        return new object();
    }

    /// <summary>
    ///     Получить информацию о конференции
    /// </summary>
    /// <returns>Информация</returns>
    public override string ToString()
    {
        var info = new StringBuilder();
        info.AppendLine($"Название конференции: {Name}");
        info.AppendLine($"Количество доступных билетов: {QuantityOfTickets}");
        info.AppendLine($"Продано билетов: {Tickets.Count}");
        info.AppendLine("---");
        foreach (var ticket in _tickets) 
            info.AppendLine($"{ticket}");
        info.AppendLine("---");
        return info.ToString();
        
    }
}