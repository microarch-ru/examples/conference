using System.Text;
using ConferenceApp.Domain.SharedKernel;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SalesContext.ConferenceAggregate;

/// <summary>
///     Билет
/// </summary>
public class Ticket : Entity<Guid>
{
    /// <summary>
    ///     Ctr
    /// </summary>
    protected Ticket()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="price">Цена</param>
    private Ticket(decimal price) : this()
    {
        Id = Guid.NewGuid();
        Price = price;
        Status = Status.Free;
    }

    /// <summary>
    ///     Цена
    /// </summary>
    public decimal Price { get; protected set; }

    /// <summary>
    ///     Статус оплаты
    /// </summary>
    public Status Status { get; protected set; }

    /// <summary>
    ///     Идентификатор покупателя
    /// </summary>
    public Guid? BuyerId { get; protected set; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="price">Цена</param>
    /// <returns>Результат</returns>
    public static Result<Ticket, Error> Create(decimal price)
    {
        if (price <= 0)
            return Errors.General.ValueIsInvalid();

        return new Ticket(price);
    }

    /// <summary>
    ///     Забронировать билет
    /// </summary>
    /// <param name="buyerId">Идентификатор покупателя</param>
    public Result<object, Error> Book(Guid buyerId)
    {
        if (buyerId == Guid.Empty)
            return Errors.General.ValueIsInvalid();
        BuyerId = buyerId;

        Status = Status.Booked;
        return new object();
    }

    /// <summary>
    ///     Оплатить билет
    /// </summary>
    public Result<object, Error> Sell()
    {
        if (Status != Status.Booked) return Errors.Ticket.TicketShouldBeBooked();
        Status = Status.Sold;
        return new object();
    }

    /// <summary>
    ///     Отменить бронь билета
    /// </summary>
    public Result<object, Error> Cancel()
    {
        Status = Status.Free;
        BuyerId = null;
        return new object();
    }

    /// <summary>
    ///     Получить информацию о билете
    /// </summary>
    /// <returns>Информация</returns>
    public override string ToString()
    {
        var info = new StringBuilder();
        info.AppendLine($"Идентификатор билета: {Id}");
        info.AppendLine($"Идентификатор покупателя: {BuyerId}");
        info.AppendLine($"Цена билета: {Price}");
        info.Append($"Статус: {Status.Name}");
        return info.ToString();
    }
}