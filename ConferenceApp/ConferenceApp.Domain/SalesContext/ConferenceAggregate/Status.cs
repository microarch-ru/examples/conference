﻿using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SalesContext.ConferenceAggregate;

/// <summary>
///     Статус билета
/// </summary>
public class Status : Entity<int>
{
    public static readonly Status Free = new(1, nameof(Free).ToLowerInvariant());
    public static readonly Status Booked = new(2, nameof(Booked).ToLowerInvariant());
    public static readonly Status Sold = new(3, nameof(Sold).ToLowerInvariant());

    /// <summary>
    ///     Ctr
    /// </summary>
    private Status(int id, string name)
    {
        Id = id;
        Name = name;
    }

    /// <summary>
    ///     Название
    /// </summary>
    public string Name { get; }

    public static IEnumerable<Status> List()
    {
        return new[] {Free, Booked, Sold};
    }

    public static Status FromName(string name)
    {
        var state = List()
            .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Status)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }

    public static Status From(int id)
    {
        var state = List().SingleOrDefault(s => s.Id == id);

        if (state == null)
            throw new ArgumentException(
                $"Possible values for {nameof(Status)}: {string.Join(",", List().Select(s => s.Name))}");

        return state;
    }
}
