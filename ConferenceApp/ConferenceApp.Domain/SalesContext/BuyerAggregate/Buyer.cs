using ConferenceApp.Domain.SharedKernel;
using ConferenceApp.Utils.SeedWork;
using CSharpFunctionalExtensions;

namespace ConferenceApp.Domain.SalesContext.BuyerAggregate;

/// <summary>
///     Покупатель
/// </summary>
public class Buyer : Entity<Guid>, IAggregateRoot
{
    /// <summary>
    ///     Ctr
    /// </summary>
    protected Buyer()
    {
    }

    /// <summary>
    ///     Ctr
    /// </summary>
    /// <param name="fio">ФИО</param>
    /// <param name="phoneNumber">Номер телефона</param>
    /// <param name="email">Email</param>
    /// <param name="address">Адрес</param>
    /// <param name="bankCard">Банковская карта</param>
    private Buyer(Fio fio, PhoneNumber phoneNumber, Email email, Address address, BankCard bankCard) : this()
    {
        Id = Guid.NewGuid();
        Fio = fio;
        PhoneNumber = phoneNumber;
        Email = email;
        Address = address;
        BankCard = bankCard;
    }

    /// <summary>
    ///     ФИО
    /// </summary>
    public Fio Fio { get; }

    /// <summary>
    ///     Номер телефона
    /// </summary>
    public PhoneNumber PhoneNumber { get; }

    /// <summary>
    ///     Email
    /// </summary>
    public Email Email { get; }

    /// <summary>
    ///     Адрес
    /// </summary>
    public Address Address { get; }

    /// <summary>
    ///     Банковская карта
    /// </summary>
    public BankCard BankCard { get; }

    /// <summary>
    ///     Factory Method
    /// </summary>
    /// <param name="fio">ФИО</param>
    /// <param name="phoneNumber">Номер телефона</param>
    /// <param name="email">Email</param>
    /// <param name="address">Адрес</param>
    /// <param name="bankCard">Банковская карта</param>
    /// <returns>Результат</returns>
    public static Result<Buyer, Error> Create(Fio fio, PhoneNumber phoneNumber, Email email, Address address,
        BankCard bankCard)
    {
        return new Buyer(fio, phoneNumber, email, address, bankCard);
    }
}