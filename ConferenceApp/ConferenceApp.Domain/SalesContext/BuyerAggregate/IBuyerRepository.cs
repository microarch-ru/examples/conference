﻿using ConferenceApp.Utils.SeedWork;

namespace ConferenceApp.Domain.SalesContext.BuyerAggregate;

/// <summary>
///     Контракт репозитория для Buyer
/// </summary>
public interface IBuyerRepository : IRepository<Buyer>
{
    Buyer Add(Buyer buyer);
    Buyer Update(Buyer buyer);
    Task<Buyer> FindAsync(Guid Id);
    Task<Buyer> FindByIdAsync(Guid Id);
}

