[![pipeline status](https://gitlab.com/microarch-ru/examples/conference/badges/main/pipeline.svg)](https://gitlab.com/microarch-ru/examples/conference/-/commits/main)

[![coverage report](https://gitlab.com/microarch-ru/examples/conference/badges/main/coverage.svg)](https://gitlab.com/microarch-ru/examples/conference/-/commits/main)

[![Latest Release](https://gitlab.com/microarch-ru/examples/conference/-/badges/release.svg)](https://gitlab.com/microarch-ru/examples/conference/-/releases)

[[_TOC_]]

# Conference
Монолит с 4мя контекстами.

<!-- ## Container diagram
Диаграмма контейнеров показывает высокоуровневую архитектуру программного обеспечения и то, как в ней распределяются обязанности. Она также показывает основные используемые технологии и то, как контейнеры взаимодействуют друг с другом. Это простая схема высокого уровня, ориентированная на технологии, которая одинаково полезна как для разработчиков программного обеспечения, так и для персонала службы поддержки и эксплуатации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services
skinparam wrapWidth 200
skinparam maxMessageSize 200
LAYOUT_TOP_DOWN()
LAYOUT_WITH_LEGEND()

!include actors/manager.puml

System_Boundary(boundary, "Warehouse") {
!include frontends/backoffice/web_app.puml
!include frontends/backoffice/gateway.puml
Rel(manager, backoffice_web_app, "Принять поставку", "HTTPS")

!include services/warehouse/normal.puml
!include services/warehouse/db.puml
Rel(backoffice_bff, warehouse, "Принять поставку", "HTTP")
}

!include services/catalog/ext.puml
Rel_L(warehouse, catalog_ext, "Добавлен новый продукт", "Async, Kafka")
Rel_L(warehouse, catalog_ext, "Изменены остатки существующего продукта", "Async, Kafka")

!include services/basket/ext.puml
Rel_L(basket_ext, warehouse, "Cоздан новый заказ", "Async, Kafka")
```
-->
## Component diagram
Диаграмма компонентов показывает, из каких «компонентов» состояит контейнер, что представляет собой каждый из этих компонентов, его обязанности, технологии и детали реализации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml
LAYOUT_WITH_LEGEND()

' Container_Ext(api_client, "API Client", " HTTP REST", "Внешний потребитель API")

' Container_Ext(message_bus, "Message Bus", "Kafka", "Сообщения")

Container_Boundary(warehouse_service, "ConferenceApp") {
    Container_Boundary(api_layer, "UI Layer") {
      Component(console, "Console App", "Terminal", "Обрабатывает запросы из командной строки")
    }

    Container_Boundary(domain_layer, "Contexts") {
      Boundary(attracting_speakers_context, "AttractingSpeakersContext") {  
        Boundary(attracting_speakers_context_domain_layer, "Domain Layer") { 
          Component(application_aggregate, "Application", "Aggregate", "Заявка на выступление")
          Component(tutor_aggregate, "Tutor", "Aggregate", "Куратор")
        }

        Boundary(attracting_speakers_context_infrastructure_layer, "Infrastructure Layer") { 
          Component(application_aggregate_repository, "ApplicationRepository", "", "Сохранение/восстановление аггрегата") 
          Component(tutor_aggregate_repository, "TutorRepository", "", "Сохранение/восстановление аггрегата") 
        }
        Lay_D(attracting_speakers_context_domain_layer, attracting_speakers_context_infrastructure_layer)
      }  

      Boundary(dining_context, "DiningContext") {  
        Boundary(dining_context_domain_layer, "Domain Layer") { 
          Component(catering_aggregate, "Catering", "Aggregate", "Кухня")
          Component(lunch_aggregate, "Lunch", "Aggregate", "Обед")
        }
        Boundary(dining_context_infrastructure_layer, "DiningContext") {  
          Component(catering_aggregate_repository, "CateringRepository", "", "Сохранение/восстановление аггрегата") 
          Component(lunch_aggregate_repository, "LunchRepository", "", "Сохранение/восстановление аггрегата") 
        }
        Lay_D(dining_context_domain_layer, dining_context_infrastructure_layer)
      }

      Boundary(sales_context, "SalesContext") {  
        Boundary(sales_context_domain_layer, "Domain Layer") { 
          Component(buyer_aggregate, "Buyer", "Aggregate", "Покупатель")
          Component(conference_aggregate, "Conference", "Aggregate", "Конференция")
        }
        Boundary(sales_context_infrastructure_layer, "SalesContext") {  
          Component(buyer_aggregate_repository, "BuyerRepository", "", "Сохранение/восстановление аггрегата") 
          Component(conference_aggregate_repository, "ConferenceRepository", "", "Сохранение/восстановление аггрегата") 
        }
        Lay_D(sales_context_domain_layer, sales_context_infrastructure_layer)
      }

      Boundary(speeches_context, "SpeechesContext") {  
        Boundary(speeches_context_domain_layer, "Domain Layer") { 
          Component(timetable_aggregate, "Timetable", "Aggregate", "Сетка конференции")
          Component(presentation_aggregate, "Presentation", "Aggregate","Доклад")
        }

        Boundary(speeches_context_infrastructure_layer, "SpeechesContext") {  
          Component(timetable_aggregate_repository, "TimetableRepository", "", "Сохранение/восстановление аггрегата") 
          Component(presentation_aggregate_repository, "PresentationRepository", "", "Сохранение/восстановление аггрегата") 
        }
        Lay_D(speeches_context_domain_layer, speeches_context_infrastructure_layer)
      }
    }

    Lay_D(api_layer, domain_layer)
    Rel(console, application_aggregate,"")
    Rel(console, tutor_aggregate,"")

    Rel(console, catering_aggregate,"")
    Rel(console, lunch_aggregate,"")

    Rel(console, buyer_aggregate,"")
    Rel(console, conference_aggregate,"")

    Rel(console, timetable_aggregate,"")
    Rel(console, presentation_aggregate,"")

    
}
``` 

## Code diagram
Диаграмма классов показывает общую структуру иерархии классов системы, их коопераций, атрибутов, методов, интерфейсов и взаимосвязей между ними.

### AttractingSpeakersContext
```plantuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/fio.iuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/phone_number.iuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/email.iuml

namespace ConferenceApp.Domain.AttractingSpeakersContext  #DDDDDD {
  Class Application <Aggregate>
  {
    - uuid Id
    - string Name
    - string Description
    - string Reason
    - Fio Fio
    - PhoneNumber PhoneNumber
    - Email Email
    - Status Status
    - Application()
    - Application(string name, string description, Fio fio, PhoneNumber phoneNumber, Email email)

    + Result<Application, Error> Create(string name, string description, Fio fio, PhoneNumber phoneNumber, Email email)
    + Result<object, Error> SubmitForReview()
    + Result<object, Error> Accept()
    + Result<object, Error> Cancel(string reason)
  }
  Application *-- SharedKernel.Fio
  Application *- SharedKernel.PhoneNumber
  Application *- SharedKernel.Email  

  Class Status <Entity>
  {
    - int Id
    - string Name
    - Status(int id, string name)

    + IEnumerable<Status> List()
    + Status FromName(string name)
    + Status From(int id)
  }
  Application *- Status

  Class Tutor <Aggregate>
  {
    - uuid Id
    - Fio Fio
    - Tutor()
    - Tutor(Fio fio)

    + Result<Tutor, Error> Create(Fio fio)
  }
  Tutor *-- SharedKernel.Fio
}
```

### DiningContext
```plantuml
namespace ConferenceApp.Domain.DiningContext #DDDDDD {
  Class Catering <Aggregate>
  {
    - uuid Id
    - List<Coupon> _coupons

    + Result<Catering, Error> Create()
    + Result<object, Error> UseCoupon(Coupon coupon)
    + Result<Coupon, Error> IssueCoupon(Guid participantId)
  }

  Class Coupon <Entity>
  {
    - int Id
    - Status Status
    - Guid? ParticipantId
    - Coupon()
    - Coupon(Guid participantId)

    + Result<Coupon, Error> Create(Guid participantId)
    + Result<object, Error> Use()
  }
  Catering *-- Coupon

  Class Status <Entity>
  {
    - int Id
    - string Name
    - Status(int id, string name)

    + IEnumerable<Status> List()
    + Status FromName(string name)
    + Status From(int id)
  }
  Coupon *-- Status

  Class Lunch <Aggregate>
  {
    - uuid Id
    - string Name
    - Course FirstCourse
    - Course SecondCourse
    - Course Drink
    - Lunch()
    - Lunch(string name, Course firstCourse, Course secondCourse, Course drink)

    + Result<Lunch, Error> Create(string name, Course firstCourse, Course secondCourse, Course drink)
  }

  Class Course <Entity>
  {
    - int Id
    - string Name
    - Course()
    - Course(string name)

    + Result<Course, Error> Create(string name)
  }
  Lunch *-- Course
}
```

### SalesContext
```plantuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/address.iuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/bank_card.iuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/email.iuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/fio.iuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/phone_number.iuml

namespace ConferenceApp.Domain.SalesContext #DDDDDD {
  Class Buyer <Aggregate>
  {
    - uuid Id
    - Fio Fio
    - PhoneNumber PhoneNumber
    - Email Email
    - Address Address
    - BankCard BankCard
    - Buyer()

    + Result<Buyer, Error> Create(Fio fio, PhoneNumber phoneNumber, Email email, Address address, BankCard bankCard)
  }
  Buyer *-- SharedKernel.Fio
  Buyer *-- SharedKernel.PhoneNumber
  Buyer *-- SharedKernel.Email
  Buyer *-- SharedKernel.Address
  Buyer *-- SharedKernel.BankCard
  

  Class Conference <Aggregate>
  {
    - uuid Id
    - string Name
    - int QuantityOfTickets
    - decimal OneTicketPrice
    - IReadOnlyCollection<Ticket> Tickets
    - Conference()

    + Result<Conference, Error> Create(string name, int quantityOfTickets, decimal oneTicketPrice)
  }
  
  Class Ticket <Entity>
  {
    - uuid Id
    - decimal Price
    - Status Status
    - Guid? BuyerId
    - Ticket()

    + Result<Ticket, Error> Create(decimal price)
    + Result<object, Error> Book(Guid buyerId)
    + Result<object, Error> Sell()
    + Result<object, Error> Cancel()
    + string ToString()
  }
  Conference *-- Ticket
  Ticket *-- Buyer

  Class Status <Entity>
  {
    - int Id
    - string Name
    - Status(int id, string name)

    + IEnumerable<Status> List()
    + Status FromName(string name)
    + Status From(int id)
  }
  Conference *-- Status  
}
```

### SpeechesContext
```plantuml
!include https://gitlab.com/microarch-ru/examples/conference/-/raw/main/shared_kernel/fio.iuml

namespace ConferenceApp.Domain.Speeches #DDDDDD {
  Class Presentation <Aggregate>
  {
    - uuid Id
    - string Name
    - string Description
    - Fio Fio
    - Status Status
    - Score Score
    - string Comment
    - Presentation()
    - Presentation(string name, string description, Fio fio)

    + Result<Presentation, Error> Create(string name, string description, Fio fio)
    + Result<object, Error> MarkAsCompleted()
    + Result<object, Error> Estimate(Score score, string comment)
  }
  Presentation *-- SharedKernel.Fio
  Presentation *-- Status
  Presentation *-- Score

  Class Status <Entity>
  {
    - int Id
    - string Name
    - Status(int id, string name)

    + IEnumerable<Status> List()
    + Status FromName(string name)
    + Status From(int id)
  }

  Class Score <Entity>
  {
    - int Id
    - string Name
    - Score(int id, string name)

    + IEnumerable<Score> List()
    + Score FromName(string name)
    + Score From(int id)
  } 
  

  Class Timetable <Aggregate>
  {
    - uuid Id
    - List<Slot> _slots

    + Result<Timetable, Error> Create()
    + Result<object, Error> TakeSlot(Slot slot, Presentation presentation)
    + Result<object, Error> ClearSlot(Slot slot)
  }
  Timetable *-- Slot
  
  Class Slot <Entity>
  {
    - uuid Id
    - int From
    - Guid? PresentationId
    - Slot()
    - Slot(int from, int to)

    + Result<Slot, Error> Create(int from, int to)
    + IEnumerable<Slot> List()
    + Result<object, Error> AddPresentation(Presentation presentation)
    + Result<object, Error> ClearSlot()
  } 
}
```




